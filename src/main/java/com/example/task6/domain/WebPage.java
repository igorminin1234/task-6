package com.example.task6.domain;

import org.springframework.lang.NonNull;

public class WebPage {

    private final String html;

    public WebPage(@NonNull final String html) {
        this.html = html;
    }

    public String receivePage() {
        return html;
    }
}
