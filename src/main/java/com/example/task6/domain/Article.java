package com.example.task6.domain;

import org.springframework.lang.NonNull;


public class Article {
    private final String html;

    public Article(@NonNull final String html) {
        this.html = html;
    }

    public String receiveArticle() {
        return html;
    }
}
