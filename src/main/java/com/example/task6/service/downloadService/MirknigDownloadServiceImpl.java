package com.example.task6.service.downloadService;

import com.example.task6.domain.Link;
import com.example.task6.domain.WebPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class MirknigDownloadServiceImpl extends AbstractDownloadService {


    @Value("${mirknig.link}")
    public String mirknigAddress;

    private Link link;

    @PostConstruct
    public void init() {
        link = new Link(mirknigAddress);

    }

    public MirknigDownloadServiceImpl() {
    }

    @Override
    public WebPage download() {
        try {
            return getHTMLFromResponse(link);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Link receiveLink() {
        return link;
    }
}
