package com.example.task6.service.downloadService;

import com.example.task6.domain.Link;
import com.example.task6.domain.WebPage;
import org.springframework.stereotype.Component;

@Component
public interface WebPageDownloadService {
    WebPage download();
    Link receiveLink();
}
