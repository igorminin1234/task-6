package com.example.task6.service.downloadService;

import com.example.task6.domain.Link;
import com.example.task6.domain.WebPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class OpennetDownloadServiceImpl extends AbstractDownloadService {


    @Value("${opennet.link}")
    public String opennetAddress;

    private Link link;

    @PostConstruct
    public void init() {
        link = new Link(opennetAddress);

    }

    public OpennetDownloadServiceImpl() {
    }

    @Override
    public WebPage download() {
        try {
            return getHTMLFromResponse(link);
        } catch (IOException | InterruptedException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Link receiveLink() {
        return link;
    }
}