package com.example.task6.service.parseService;

import com.example.task6.domain.Article;
import com.example.task6.domain.Regexp;
import com.example.task6.domain.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class MirknigArticleParserImpl implements ParseArticleService {


    @Value("${mirknig.pattern.today}")
    public String mirknigTodayPattern;

    @Value("${mirknig.pattern.path.to.article}")
    public String mirknigPathToArticlePattern;

    @Value("${mirknig.pattern.path.to.date}")
    public String mirknigPathToDatePattern;

    @Value("${mirknig.pattern.path.to.heading}")
    public String mirknigPathToHeadingPattern;

    private Regexp todayPattern;
    private Regexp pathToArticlePattern;
    private Regexp pathToDatePattern;
    private Regexp pathToHeadingPattern;

    @PostConstruct
    public void init() {
        this.todayPattern = new Regexp(mirknigTodayPattern);
        this.pathToArticlePattern = new Regexp(mirknigPathToArticlePattern);
        this.pathToDatePattern = new Regexp(mirknigPathToDatePattern);
        this.pathToHeadingPattern = new Regexp(mirknigPathToHeadingPattern);
    }

    public MirknigArticleParserImpl() {

    }

    @Override
    public List<Article> parse(final WebPage html) {
        final Document doc = Jsoup.parse(html.receivePage());
        final List<Article> articles = new ArrayList<>();

        final Elements elems = doc.select(pathToArticlePattern.receiveRegexp());
        for (Element elem : elems) {
            if (elem.select(pathToDatePattern.receiveRegexp()).text().contains(todayPattern.receiveRegexp())) {
                articles.add(new Article(elem.select(pathToHeadingPattern.receiveRegexp()).text()));
            }
        }
        return articles;
    }
}
