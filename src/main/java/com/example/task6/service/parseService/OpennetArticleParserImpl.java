package com.example.task6.service.parseService;

import com.example.task6.domain.Article;
import com.example.task6.domain.DateFormat;
import com.example.task6.domain.Regexp;
import com.example.task6.domain.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class OpennetArticleParserImpl implements ParseArticleService {


    @Value("${opennet.pattern.today}")
    public String opennetPatternToday;

    @Value("${opennet.date.format}")
    public String opennetDateFormatStr;

    private Regexp regexp;
    private DateFormat dateFormat;

    @PostConstruct
    public void init() {
        regexp = new Regexp(opennetPatternToday);
        dateFormat = new DateFormat(opennetDateFormatStr);
    }

    public OpennetArticleParserImpl() {

    }

    private String changeDateFormat(final LocalDate startDateFormat) {
        return new SimpleDateFormat(dateFormat.receiveDateFormat())
                .format(Date.from(startDateFormat.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public List<Article> parse(final WebPage html) {
        final Document doc = Jsoup.parse(html.receivePage());
        final Elements dates = doc.select("tr>td.tdate");
        final List<Article> articles = new ArrayList<>();

        for (final Element date : dates) {
            if (date.toString().equals(regexp
                    .receiveRegexp()
                    .replaceAll("changeFormatDate", changeDateFormat(LocalDate.now())) //replace for change date format from yyyy-mm-dd to dd.mm.yyyy
            )) {
                articles.add(new Article(date.parent().select("td.titlebr>a").text()));
            }
        }
        return articles;
    }


}
