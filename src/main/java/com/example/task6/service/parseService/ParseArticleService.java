package com.example.task6.service.parseService;

import com.example.task6.domain.Article;
import com.example.task6.domain.WebPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ParseArticleService {
    List<Article> parse(WebPage html);
}
