package com.example.task6.app.controller;

import com.example.task6.domain.Article;
import com.example.task6.domain.Link;
import com.example.task6.domain.WebPage;
import com.example.task6.service.downloadService.MirknigDownloadServiceImpl;
import com.example.task6.service.downloadService.OpennetDownloadServiceImpl;
import com.example.task6.service.downloadService.WebPageDownloadService;
import com.example.task6.service.parseService.MirknigArticleParserImpl;
import com.example.task6.service.parseService.OpennetArticleParserImpl;
import com.example.task6.service.parseService.ParseArticleService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@org.springframework.stereotype.Controller
@RequestMapping("/")
public class Controller {

    private final WebPageDownloadService opennetDownloader;

    private final WebPageDownloadService mirknigDownloader;

    private final ParseArticleService opennetParser;

    private final ParseArticleService mirknigParser;

    public Controller(final OpennetDownloadServiceImpl opennetDownloader,
                      final MirknigDownloadServiceImpl mirknigDownloader,
                      final OpennetArticleParserImpl opennetParser,
                      final MirknigArticleParserImpl mirknigParser) {
        this.opennetDownloader = opennetDownloader;
        this.mirknigDownloader = mirknigDownloader;
        this.opennetParser = opennetParser;
        this.mirknigParser = mirknigParser;
    }

    @GetMapping("/")
    public String openMainPage() {
        return "main";
    }

    @PostMapping("/")
    public String showTodaysArticles(@RequestParam(name = "website") final String websiteName, final Model model) {
        final WebPage page;
        final List<Article> articles;
        final Link link;
        if (websiteName.equals("opennet")) {
            page = opennetDownloader.download();
            System.out.println(page);
            articles = opennetParser.parse(page);
            System.out.println(articles);
            link = opennetDownloader.receiveLink();

        } else if (websiteName.equals("mirknig")) {
            page = mirknigDownloader.download();
            articles = mirknigParser.parse(page);
            link = mirknigDownloader.receiveLink();
        } else {
            return "error";
        }

        model.addAttribute("headings", articles);
        model.addAttribute("link", link.receiveLinkAddress());

        return "articles";
    }
}
